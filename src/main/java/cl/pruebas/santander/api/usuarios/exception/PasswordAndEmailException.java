package cl.pruebas.santander.api.usuarios.exception;

public class PasswordAndEmailException extends RuntimeException{

	/**
	 * 
	 */
	private static final long serialVersionUID = -3156628049830091903L;

	public PasswordAndEmailException(String message) {
        super(message);
    }

    public PasswordAndEmailException(String message, Throwable cause) {
        super(message, cause);
    }
}
