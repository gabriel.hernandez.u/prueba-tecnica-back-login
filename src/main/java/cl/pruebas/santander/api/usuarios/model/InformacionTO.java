package cl.pruebas.santander.api.usuarios.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class InformacionTO {

	private String codigoOperacion;
	private String glosaOperacion;


}
