package cl.pruebas.santander.api.usuarios.service;

import java.util.Optional;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cl.pruebas.santander.api.usuarios.entity.UsuarioEntity;
import cl.pruebas.santander.api.usuarios.exception.AlreadyExistsUserException;
import cl.pruebas.santander.api.usuarios.exception.PasswordAndEmailException;
import cl.pruebas.santander.api.usuarios.model.InformacionTO;
import cl.pruebas.santander.api.usuarios.model.LoginRespTO;
import cl.pruebas.santander.api.usuarios.model.UsuarioLoginReqTO;
import cl.pruebas.santander.api.usuarios.model.UsuarioReqTO;
import cl.pruebas.santander.api.usuarios.model.UsuarioRespTO;
import cl.pruebas.santander.api.usuarios.repository.UsuarioRepository;
import cl.pruebas.santander.api.usuarios.utils.JwtUtils;
import cl.pruebas.santander.api.usuarios.utils.Utilidades;
import cl.pruebas.santander.api.usuarios.wrapper.UsuarioWrapper;
import lombok.extern.slf4j.Slf4j;

/**
 *
 * @author Gabriel Hernandez
 */
@Slf4j
@Service
public class UsuarioService {

	private Utilidades util;

	private JwtUtils jwtUtil;

	private UsuarioRepository repository;

	private UsuarioWrapper wrapper;

	@Autowired
	public UsuarioService(Utilidades util, JwtUtils jwtUtil, UsuarioRepository repository, UsuarioWrapper wrapper) {
		this.jwtUtil = jwtUtil;
		this.repository = repository;
		this.util = util;
		this.wrapper = wrapper;

	}

	/**
	 * Obtiene el usuario desde BD
	 *
	 * @return
	 */

	public LoginRespTO refrescartoken(UsuarioLoginReqTO request) {
		LoginRespTO resp = new LoginRespTO();

		Optional<UsuarioEntity> consultaEntidad = repository.findById(UUID.fromString(request.getId()));

		String tokenNew = jwtUtil.getJWTToken(consultaEntidad.get().getUser());
		consultaEntidad.get().setToken(tokenNew);

		UsuarioEntity entidadActualizada = repository.save(consultaEntidad.get());
		resp = wrapper.getLoginRespTO(entidadActualizada);

		return resp;
	}

	public LoginRespTO consultarUsuario(UsuarioLoginReqTO request) {

		Optional<UsuarioEntity> opt = repository.findById(UUID.fromString(request.getId()));

		return wrapper.getLoginRespTO(opt.get());

	}

	public UsuarioRespTO crearUsuario(UsuarioReqTO request) throws PasswordAndEmailException {
		UsuarioRespTO resp = new UsuarioRespTO();
		log.info("request-->{}", request);
		InformacionTO error = validaciones(request);
		log.info("retorno validaciones-->{}", error);
		resp = grabarUsuario(request);
		log.info("retorno grabar usuario-->{}", resp);

		return resp;
	}

	public UsuarioRespTO grabarUsuario(UsuarioReqTO request) throws AlreadyExistsUserException {
		log.info("entro grabar usuario-->{}", request);
		InformacionTO error = new InformacionTO();
		UsuarioEntity uus = wrapper.getUsuarioEntity(request);

		UsuarioEntity uus2 = repository.findByNameAndPassword(request.getUser(), request.getPassword());

		if (uus2 != null) {

			throw new AlreadyExistsUserException("Usuario ya existe en el sistema");
		} else {

			UsuarioEntity entidadGrb = repository.save(uus);

			UsuarioRespTO resp = wrapper.getUsuarioRespTO(entidadGrb);
			error.setCodigoOperacion("000");
			error.setGlosaOperacion("operación realizada con exito hola " + request.getUser());
			resp.setInformation(error);
			return resp;
		}

	}

	public InformacionTO validaciones(UsuarioReqTO request) throws PasswordAndEmailException {
		boolean emailValido = false;
		boolean passwordCumpleformato = false;
		InformacionTO error = new InformacionTO();

		emailValido = util.isValidEmailAddress(request.getEmail());
		passwordCumpleformato = util.cumpleFormatoPwd(request.getPassword());

		if (!emailValido) {

			error.setCodigoOperacion("001");
			error.setGlosaOperacion("email con formato erroneo");
			throw new PasswordAndEmailException(error.getGlosaOperacion());

		} else if (!passwordCumpleformato) {

			error.setCodigoOperacion("002");
			error.setGlosaOperacion("password no corresponde formato");
			throw new PasswordAndEmailException(error.getGlosaOperacion());
		} else {
			error.setCodigoOperacion("000");
			error.setGlosaOperacion("");
		}

		return error;
	}
}
