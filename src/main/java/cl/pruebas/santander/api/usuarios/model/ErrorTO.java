package cl.pruebas.santander.api.usuarios.model;

import java.io.Serializable;
import java.time.LocalDateTime;

import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ErrorTO implements Serializable {

	/**
	* 
	*/
	private static final long serialVersionUID = 4361121742550663207L;
	
	@JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
	private LocalDateTime timeStamp;
	private Integer codigo;
	private String detail;

}
