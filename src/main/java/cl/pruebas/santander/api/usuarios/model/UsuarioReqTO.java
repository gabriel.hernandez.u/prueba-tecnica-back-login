package cl.pruebas.santander.api.usuarios.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.springframework.validation.annotation.Validated;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * Periodo
 */
@Validated
@AllArgsConstructor
@NoArgsConstructor
@Data
@EqualsAndHashCode
public class UsuarioReqTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3119503802492625687L;

	@JsonProperty("user")
	private String user = null;
	
	@NotNull
	@JsonProperty("email")
	private String email = null;

	@NotNull
	@JsonProperty("password")
	private String password = null;

	@JsonProperty("phones")
	@Valid
	private List<Phones> phones = new ArrayList<Phones>();

}
