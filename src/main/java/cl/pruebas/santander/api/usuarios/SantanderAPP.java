package cl.pruebas.santander.api.usuarios;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import cl.pruebas.santander.api.usuarios.jwt.JWTAuthorizationFilter;

@SpringBootApplication
public class SantanderAPP {

	public static void main(String[] args) {
		SpringApplication.run(SantanderAPP.class, args);
	}

	@EnableWebSecurity
	@Configuration
	class WebSecurityConfig extends WebSecurityConfigurerAdapter {

		@Override
		protected void configure(HttpSecurity http) throws Exception {
			http.csrf().disable()
					.addFilterAfter(new JWTAuthorizationFilter(), UsernamePasswordAuthenticationFilter.class)
					.authorizeRequests()
					.antMatchers(HttpMethod.POST, "/sign-up").permitAll()
					.antMatchers(HttpMethod.POST, "/refreshToken").permitAll()
					.antMatchers(HttpMethod.POST, "/h2-console/*").permitAll()
					.antMatchers(HttpMethod.POST, "/v2/*").permitAll()
					.antMatchers(HttpMethod.POST, "/login").authenticated();

		}
	}

}