package cl.pruebas.santander.api.usuarios.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UsuarioLoginReqTO {

	
	private String id;
	private String user;
	private String password;
	

}
