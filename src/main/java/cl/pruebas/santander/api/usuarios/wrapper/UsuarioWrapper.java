package cl.pruebas.santander.api.usuarios.wrapper;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import cl.pruebas.santander.api.usuarios.entity.UsuarioEntity;
import cl.pruebas.santander.api.usuarios.entity.UsuarioPhonesEntity;
import cl.pruebas.santander.api.usuarios.model.LoginRespTO;
import cl.pruebas.santander.api.usuarios.model.Phones;
import cl.pruebas.santander.api.usuarios.model.UsuarioReqTO;
import cl.pruebas.santander.api.usuarios.model.UsuarioRespTO;
import cl.pruebas.santander.api.usuarios.repository.UserPhonesRespository;
import cl.pruebas.santander.api.usuarios.utils.JwtUtils;
import cl.pruebas.santander.api.usuarios.utils.Utilidades;

@Component
public class UsuarioWrapper {

	private Utilidades util;
	private JwtUtils jwtUtils;

	@Autowired
	public UsuarioWrapper(Utilidades util, JwtUtils jwtUtils, UserPhonesRespository repoPhonos) {
		this.jwtUtils = jwtUtils;
		this.util = util;

	}

	public UsuarioEntity getUsuarioEntity(UsuarioReqTO request) {
		UUID uuid = UUID.randomUUID();
		UsuarioEntity user = new UsuarioEntity();
		user.setActive(true);
		user.setCreated(LocalDateTime.now());
		user.setEmail(request.getEmail());
		user.setId(uuid);
		user.setLastLogin(LocalDateTime.now());
		user.setUser(request.getUser());
		user.setPassword(util.encriptarPwd(request.getPassword()));
		user.setToken(jwtUtils.getJWTToken(request.getUser()));
		if (!request.getPhones().isEmpty()) {
			List<UsuarioPhonesEntity> phones = new ArrayList<>();

			request.getPhones().stream().forEach((p) -> {
				UsuarioPhonesEntity uph = new UsuarioPhonesEntity();
				uph.setUsuario(user);
				uph.setCiudad(p.getCitycode());
				uph.setNumber(p.getNumber());
				uph.setPais(p.getCountrycode());
				phones.add(uph);
			});

			user.setPhones(phones);
		}

		return user;

	}

	public UsuarioRespTO getUsuarioRespTO(UsuarioEntity entidad) {
		UsuarioRespTO resp = new UsuarioRespTO();

		resp.setCreated(entidad.getCreated());
		resp.setId(entidad.getId().toString());
		resp.setIsActive(entidad.isActive());
		resp.setLastLogin(entidad.getLastLogin());
		resp.setToken(entidad.getToken());

		return resp;

	}

	public LoginRespTO getLoginRespTO(UsuarioEntity usuario) {

		LoginRespTO login = new LoginRespTO();
		login.setActive(usuario.isActive());
		login.setCreated(usuario.getCreated());
		login.setEmail(usuario.getEmail());
		login.setId(usuario.getId().toString());
		login.setLastLogin(usuario.getLastLogin());
		login.setName(usuario.getUser());
		login.setPassword(usuario.getPassword());
		login.setToken(usuario.getToken());

		List<Phones> phones = new ArrayList<>();
		for (UsuarioPhonesEntity pe : usuario.getPhones()) {
			Phones fono = new Phones();
			fono.setCitycode(pe.getCiudad());
			fono.setCountrycode(pe.getPais());
			fono.setNumber(pe.getNumber());
			phones.add(fono);
		}
		;

		login.setPhones(phones);

		return login;

	}
	
	
	public UsuarioRespTO getUsuarioRespTOFromReqTO(UsuarioReqTO req) {
		UsuarioRespTO resp = new UsuarioRespTO();
		return resp;
		
	}

}
