package cl.pruebas.santander.api.usuarios.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import cl.pruebas.santander.api.usuarios.entity.UsuarioPhonesEntity;

public interface UserPhonesRespository extends CrudRepository<UsuarioPhonesEntity, Long> {

	@Query("SELECT u FROM UsuarioPhonesEntity u WHERE u.id = :id and u.usuario= :idUsuario")
	List<UsuarioPhonesEntity> findByIdAndIdUsuario(Long id, String idUsuario);

}
