package cl.pruebas.santander.api.usuarios.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
@Data
@Entity
@Table(name = "usu_fono")
public class UsuarioPhonesEntity implements Serializable {

	private static final long serialVersionUID = 2153452977250782645L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;
	
	
	@ManyToOne
	@JoinColumn(name= "user_id")
	private UsuarioEntity usuario;

	@Column(name = "numero")
	private Long number;
	
	@Column(name = "ciudad")
	private String ciudad;

	@Column(name = "pais")
	private String pais;

}
