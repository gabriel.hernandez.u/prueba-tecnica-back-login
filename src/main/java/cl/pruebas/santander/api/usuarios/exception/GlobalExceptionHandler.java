package cl.pruebas.santander.api.usuarios.exception;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import cl.pruebas.santander.api.usuarios.model.ErrorReqTO;
import cl.pruebas.santander.api.usuarios.model.ErrorTO;
import io.jsonwebtoken.JwtException;

@ControllerAdvice
public class GlobalExceptionHandler {

	@ExceptionHandler(NumberFormatException.class)
	public ResponseEntity<Object> handleNumberFormatException(NumberFormatException ex) {
		return new ResponseEntity<>(getBody(HttpStatus.BAD_REQUEST, ex, "Please enter a valid value"),
				new HttpHeaders(), HttpStatus.BAD_REQUEST);
	}

	@ExceptionHandler(IllegalArgumentException.class)
	public ResponseEntity<Object> handleIllegalArgumentException(IllegalArgumentException ex) {
		return new ResponseEntity<>(getBody(HttpStatus.BAD_REQUEST, ex, ex.getMessage()), new HttpHeaders(),
				HttpStatus.BAD_REQUEST);
	}

	@ExceptionHandler(AccessDeniedException.class)
	public ResponseEntity<Object> handleAccessDeniedException(AccessDeniedException ex) {
		return new ResponseEntity<>(getBody(HttpStatus.FORBIDDEN, ex, ex.getMessage()), new HttpHeaders(),
				HttpStatus.FORBIDDEN);
	}

	@ExceptionHandler(Exception.class)
	public ResponseEntity<Object> exception(Exception ex) {
		return new ResponseEntity<>(getBody(HttpStatus.INTERNAL_SERVER_ERROR, ex, ex.toString()), new HttpHeaders(),
				HttpStatus.INTERNAL_SERVER_ERROR);
	}
	
	@ExceptionHandler(JwtException.class)
	public ResponseEntity<Object> exceptionJwt(JwtException ex) {
		return new ResponseEntity<>(getBody(HttpStatus.INTERNAL_SERVER_ERROR, ex, ex.toString()), new HttpHeaders(),
				HttpStatus.INTERNAL_SERVER_ERROR);
	}
	
	@ExceptionHandler(PasswordAndEmailException.class)
	public ResponseEntity<Object> exceptionEmail(PasswordAndEmailException ex) {
		return new ResponseEntity<>(getBody(HttpStatus.UNPROCESSABLE_ENTITY, ex, ex.toString()), new HttpHeaders(),
				HttpStatus.UNAUTHORIZED);
	}
	
	@ExceptionHandler(AlreadyExistsUserException.class)
	public ResponseEntity<Object> exceptionExistUser(AlreadyExistsUserException ex) {
		return new ResponseEntity<>(getBody(HttpStatus.CONFLICT, ex, ex.toString()), new HttpHeaders(),
				HttpStatus.CONFLICT);
	}

	public ErrorReqTO getBody(HttpStatus status, Exception ex, String message) {
		ErrorReqTO errorReturn = new ErrorReqTO();
		ErrorTO error = new ErrorTO();
		List<ErrorTO> errores = new ArrayList<>();

		error.setCodigo(status.value());
		error.setDetail(message);
		error.setTimeStamp(LocalDateTime.now());

		errores.add(error);

		errorReturn.setError(errores);
		return errorReturn;
	}
}