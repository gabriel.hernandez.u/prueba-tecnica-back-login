package cl.pruebas.santander.api.usuarios.exception;

public class AlreadyExistsUserException extends RuntimeException{

	/**
	 * 
	 */
	private static final long serialVersionUID = -6221857214700164458L;

		

	public AlreadyExistsUserException(String message) {
        super(message);
    }

    public AlreadyExistsUserException(String message, Throwable cause) {
        super(message, cause);
    }
}
