package cl.pruebas.santander.api.usuarios.repository;


import java.util.Optional;
import java.util.UUID;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import cl.pruebas.santander.api.usuarios.entity.UsuarioEntity;

public interface UsuarioRepository extends CrudRepository<UsuarioEntity, UUID> {

	Optional<UsuarioEntity> findById(UUID id);

	@Query("SELECT u FROM UsuarioEntity u WHERE u.user = :user and u.password= :password")
	UsuarioEntity findByNameAndPassword(@Param("user") String user, @Param("password") String password);

}