package cl.pruebas.santander.api.usuarios.utils;

import java.security.Key;
import java.util.Date;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

import javax.crypto.spec.SecretKeySpec;
import javax.xml.bind.DatatypeConverter;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.stereotype.Component;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.JwtBuilder;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

@Component
public class JwtUtils {

	public static final String SECRET_KEY = "D1686901CD4F848691EB10F756FF6F5315BFE5320A6B96C39622A3B8E3550D3B";
	
	public String createJWT(String id, String issuer, String subject, long ttlMillis) {
		  

	    SignatureAlgorithm signatureAlgorithm = SignatureAlgorithm.HS256;

	    long nowMillis = System.currentTimeMillis();
	    Date now = new Date(nowMillis);


	    byte[] apiKeySecretBytes = DatatypeConverter.parseBase64Binary(SECRET_KEY);
	    Key signingKey = new SecretKeySpec(apiKeySecretBytes, signatureAlgorithm.getJcaName());


	    JwtBuilder builder = Jwts.builder().setId(id)
	            .setIssuedAt(now)
	            .setSubject(subject)
	            .setIssuer(issuer)
	            .signWith(signatureAlgorithm, signingKey);
	  

	    if (ttlMillis > 0) {
	        long expMillis = nowMillis + ttlMillis;
	        Date exp = new Date(expMillis);
	        builder.setExpiration(exp);
	    }  
	  

	    return builder.compact();
	}

	public String getJWTToken(String username) {
		
		List<GrantedAuthority> grantedAuthorities = AuthorityUtils
				.commaSeparatedStringToAuthorityList("ROLE_USER");
		
		return Jwts
				.builder()
				.setId("globalLogic")
				.setSubject(username)
				.claim("authorities",
						grantedAuthorities.stream()
								.map(GrantedAuthority::getAuthority)
								.collect(Collectors.toList()))
				.setIssuedAt(new Date(System.currentTimeMillis()))
				.setExpiration(new Date(System.currentTimeMillis() + 600000))
				.signWith(SignatureAlgorithm.HS512,
						SECRET_KEY.getBytes()).compact();
	}
	
	public boolean isTokenExpired(String token) {
	    return extractExpiration(token).before(new Date());
	}
	
	public Date extractExpiration(String token) {
	    return extractClaim(token, Claims::getExpiration);
	}


	public <T> T extractClaim(String token , Function<Claims, T> claimResolver) {
	    final Claims claim= extractAllClaims(token);
	    return claimResolver.apply(claim);
	}


	private Claims extractAllClaims(String token) {
		
		
		
	    return (Claims) Jwts.parser().setSigningKey(SECRET_KEY).parse(token).getBody();
	}

}
