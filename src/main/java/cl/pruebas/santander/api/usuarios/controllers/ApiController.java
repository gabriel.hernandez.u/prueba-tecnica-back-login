package cl.pruebas.santander.api.usuarios.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cl.pruebas.santander.api.usuarios.model.LoginRespTO;
import cl.pruebas.santander.api.usuarios.model.UsuarioLoginReqTO;
import cl.pruebas.santander.api.usuarios.model.UsuarioReqTO;
import cl.pruebas.santander.api.usuarios.model.UsuarioRespTO;
import cl.pruebas.santander.api.usuarios.service.UsuarioService;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RequestMapping("/api/auth")
public class ApiController {

	private UsuarioService usuarioService;

	@Autowired
	public ApiController(UsuarioService usuarioService) {
		this.usuarioService = usuarioService;
	}

	@PostMapping(path = "/refreshToken", produces = MediaType.APPLICATION_JSON_VALUE)

	public ResponseEntity<LoginRespTO> refrescarToken(@RequestBody UsuarioLoginReqTO request) {

		LoginRespTO resp = usuarioService.refrescartoken(request);
		return new ResponseEntity<>(resp, HttpStatus.OK);

	}

	@PostMapping(path = "/login", produces = MediaType.APPLICATION_JSON_VALUE)

	public ResponseEntity<LoginRespTO> login(@RequestBody UsuarioLoginReqTO request) {
		LoginRespTO resp = usuarioService.consultarUsuario(request);
		return new ResponseEntity<>(resp, HttpStatus.OK);

	}

	@PostMapping(path = "/sign-up", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<UsuarioRespTO> crearUsuario(@RequestBody UsuarioReqTO request) {
		log.info("entro a crear rut datos : {}", request);

		UsuarioRespTO resp = usuarioService.crearUsuario(request);

		if (resp.getInformation().getCodigoOperacion().equalsIgnoreCase("000")) {
			return new ResponseEntity<>(resp, HttpStatus.OK);
		} else {
			return new ResponseEntity<>(resp, HttpStatus.BAD_REQUEST);
		}

	}

}
