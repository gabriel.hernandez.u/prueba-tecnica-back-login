package cl.pruebas.santander.api.usuarios.model;

import java.util.ArrayList;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ErrorReqTO {

	private List<ErrorTO> error = new ArrayList<>();

}
