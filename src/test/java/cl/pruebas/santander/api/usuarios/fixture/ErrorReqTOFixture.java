package cl.pruebas.santander.api.usuarios.fixture;

import java.util.ArrayList;
import java.util.List;

import cl.pruebas.santander.api.usuarios.model.ErrorReqTO;
import cl.pruebas.santander.api.usuarios.model.ErrorTO;

public class ErrorReqTOFixture {

	static List<ErrorTO> list = new ArrayList<>();

	public static ErrorReqTO errorReqTO() {
		return ErrorReqTO.builder().error(list).build();

	}

	public static ErrorReqTO crearErrorReqTO() {
		ErrorReqTO error = new ErrorReqTO();
		error.setError(list);
		return error;
	}

}
