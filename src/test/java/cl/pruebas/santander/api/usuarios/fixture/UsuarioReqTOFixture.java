package cl.pruebas.santander.api.usuarios.fixture;

import java.util.ArrayList;
import java.util.List;

import cl.pruebas.santander.api.usuarios.model.Phones;
import cl.pruebas.santander.api.usuarios.model.UsuarioReqTO;

public class UsuarioReqTOFixture {

	public static  UsuarioReqTO obtenerUsuarioSinPhones() {
		UsuarioReqTO fixture= new UsuarioReqTO();
		fixture.setEmail("gabriel@prueb.com");
		fixture.setUser("gabriel");
		fixture.setPassword("dasdasdasdasads");
		return fixture;
	}
	
	public static  UsuarioReqTO obtenerUsuarioConPhones() {
		List<Phones> phones = new ArrayList<>();
		phones.add(new Phones(123L, "abc", "cde"));
		UsuarioReqTO fixture= new UsuarioReqTO();
		fixture.setEmail("gabriel@prueb.com");
		fixture.setUser("gabriel");
		fixture.setPassword("sfda4sdFs4da");
		fixture.setPhones(phones);
		return fixture;
	}

}
