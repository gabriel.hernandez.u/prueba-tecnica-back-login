package cl.pruebas.santander.api.usuarios.fixture;

import java.time.LocalDateTime;

import cl.pruebas.santander.api.usuarios.model.InformacionTO;
import cl.pruebas.santander.api.usuarios.model.UsuarioRespTO;

public class UsuarioRespTOFixture {

	public static UsuarioRespTO obtenerUsuarioRespTO() {
		UsuarioRespTO fixture = new UsuarioRespTO();
		InformacionTO info = new InformacionTO("000", "exito");
		fixture.setCreated(LocalDateTime.now());
		fixture.setId("sasdasdasdasda");
		fixture.setInformation(info);
		fixture.setIsActive(true);
		fixture.setLastLogin(LocalDateTime.now());
		fixture.setToken("asdasdasdasdasdasdasdasd");
		
		return fixture;
		
	}
	
	public static UsuarioRespTO obtenerUsuarioRespTONull() {
		UsuarioRespTO fixture = new UsuarioRespTO();
		InformacionTO info = new InformacionTO("000", "ok");
		fixture.setInformation(info);
		
		return fixture;
		
	}

}
