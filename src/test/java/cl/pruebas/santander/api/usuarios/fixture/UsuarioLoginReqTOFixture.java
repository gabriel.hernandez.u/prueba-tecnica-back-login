package cl.pruebas.santander.api.usuarios.fixture;

import java.util.UUID;

import cl.pruebas.santander.api.usuarios.model.UsuarioLoginReqTO;

public class UsuarioLoginReqTOFixture {

	public static UsuarioLoginReqTO obtenerUsuarioLoginReq() {

		UsuarioLoginReqTO fixture = new UsuarioLoginReqTO();
		fixture.setId(UUID.randomUUID().toString());
		return fixture;
	}

}
