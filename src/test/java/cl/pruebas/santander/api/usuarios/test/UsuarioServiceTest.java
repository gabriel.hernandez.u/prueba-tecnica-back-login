package cl.pruebas.santander.api.usuarios.test;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.UUID;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.boot.test.context.SpringBootTest;

import cl.pruebas.santander.api.usuarios.entity.UsuarioEntity;
import cl.pruebas.santander.api.usuarios.exception.AlreadyExistsUserException;
import cl.pruebas.santander.api.usuarios.exception.PasswordAndEmailException;
import cl.pruebas.santander.api.usuarios.fixture.LoginRespTOFixture;
import cl.pruebas.santander.api.usuarios.fixture.UsuarioEntityFixture;
import cl.pruebas.santander.api.usuarios.fixture.UsuarioLoginReqTOFixture;
import cl.pruebas.santander.api.usuarios.fixture.UsuarioReqTOFixture;
import cl.pruebas.santander.api.usuarios.fixture.UsuarioRespTOFixture;
import cl.pruebas.santander.api.usuarios.repository.UsuarioRepository;
import cl.pruebas.santander.api.usuarios.service.UsuarioService;
import cl.pruebas.santander.api.usuarios.utils.JwtUtils;
import cl.pruebas.santander.api.usuarios.utils.Utilidades;
import cl.pruebas.santander.api.usuarios.wrapper.UsuarioWrapper;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@RunWith(MockitoJUnitRunner.Silent.class)
@SpringBootTest
public class UsuarioServiceTest {

	@InjectMocks
	private UsuarioService usuarioService;

	@Mock
	private UsuarioRepository repository;

	@Mock
	private JwtUtils jwtUtil;

	@Mock
	private Utilidades utils;

	@Mock
	private UsuarioWrapper wrapper;

	@Before
	public void setUp() throws Exception {

		MockitoAnnotations.initMocks(this);
		utils = mock(Utilidades.class);
		repository = mock(UsuarioRepository.class);
		jwtUtil = mock(JwtUtils.class);
		usuarioService = mock(UsuarioService.class);
		usuarioService = new UsuarioService(utils, jwtUtil, repository, wrapper);

//		 ReflectionTestUtils.setField(usuarioService, "repoUsuario", repoUsuario);
		log.info("runing setup");

	}

	@Test(expected = PasswordAndEmailException.class)
	public void addUsuariosWithEmailInvalid() throws Exception {
		when(utils.cumpleFormatoPwd(anyString())).thenReturn(true);
		when(utils.isValidEmailAddress(anyString())).thenReturn(false);
		when(wrapper.getUsuarioEntity(UsuarioReqTOFixture.obtenerUsuarioConPhones()))
				.thenReturn(UsuarioEntityFixture.obtenerEntidadUsuario().get());
		when(repository.findByNameAndPassword(anyString(), anyString())).thenReturn(null);
		when(repository.save(any(UsuarioEntity.class))).thenReturn(UsuarioEntityFixture.obtenerEntidadUsuario().get());

		when(wrapper.getUsuarioRespTO(any(UsuarioEntity.class)))
				.thenReturn(UsuarioRespTOFixture.obtenerUsuarioRespTO());

		when(usuarioService.crearUsuario(UsuarioReqTOFixture.obtenerUsuarioConPhones()))
				.thenThrow(PasswordAndEmailException.class).thenReturn(UsuarioRespTOFixture.obtenerUsuarioRespTO());

	}

	@Test(expected = PasswordAndEmailException.class)
	public void addUsuariosWithPasswordInvalid() throws Exception {
		when(utils.cumpleFormatoPwd(anyString())).thenReturn(false);
		when(utils.isValidEmailAddress(anyString())).thenReturn(true);
		when(wrapper.getUsuarioEntity(UsuarioReqTOFixture.obtenerUsuarioConPhones()))
				.thenReturn(UsuarioEntityFixture.obtenerEntidadUsuario().get());
		when(repository.findByNameAndPassword(anyString(), anyString())).thenReturn(null);
		when(repository.save(any(UsuarioEntity.class))).thenReturn(UsuarioEntityFixture.obtenerEntidadUsuario().get());

		when(wrapper.getUsuarioRespTO(any(UsuarioEntity.class)))
				.thenReturn(UsuarioRespTOFixture.obtenerUsuarioRespTO());

		when(usuarioService.crearUsuario(UsuarioReqTOFixture.obtenerUsuarioConPhones()))
				.thenThrow(PasswordAndEmailException.class).thenReturn(UsuarioRespTOFixture.obtenerUsuarioRespTO());

	}

	@Test(expected = AlreadyExistsUserException.class)
	public void addUsuarioExist() throws Exception {
		when(utils.cumpleFormatoPwd(anyString())).thenReturn(true);
		when(utils.isValidEmailAddress(anyString())).thenReturn(true);
		when(wrapper.getUsuarioEntity(UsuarioReqTOFixture.obtenerUsuarioConPhones()))
				.thenReturn(UsuarioEntityFixture.obtenerEntidadUsuario().get());
		when(repository.findByNameAndPassword(anyString(), anyString())).thenReturn(UsuarioEntityFixture.obtenerEntidadUsuario().get());
		when(usuarioService.crearUsuario(UsuarioReqTOFixture.obtenerUsuarioConPhones()))
		.thenThrow(PasswordAndEmailException.class).thenReturn(UsuarioRespTOFixture.obtenerUsuarioRespTO());
		

	}
	
	@Test
	public void addUsuariosWithNull() throws Exception {
		when(utils.cumpleFormatoPwd(anyString())).thenReturn(true);
		when(utils.isValidEmailAddress(anyString())).thenReturn(true);
		when(wrapper.getUsuarioEntity(UsuarioReqTOFixture.obtenerUsuarioConPhones()))
				.thenReturn(UsuarioEntityFixture.obtenerEntidadUsuario().get());
		when(repository.findByNameAndPassword(anyString(), anyString())).thenReturn(null);
		when(repository.save(any(UsuarioEntity.class))).thenReturn(UsuarioEntityFixture.obtenerEntidadUsuario().get());

		when(wrapper.getUsuarioRespTO(any(UsuarioEntity.class)))
				.thenReturn(UsuarioRespTOFixture.obtenerUsuarioRespTO());

		when(usuarioService.crearUsuario(UsuarioReqTOFixture.obtenerUsuarioConPhones()))
				.thenThrow(PasswordAndEmailException.class).thenReturn(UsuarioRespTOFixture.obtenerUsuarioRespTO());

	}

	@Test
	public void testLogin() throws Exception {
		when(repository.findById(any(UUID.class))).thenReturn(UsuarioEntityFixture.obtenerEntidadUsuario());

		when(wrapper.getLoginRespTO(UsuarioEntityFixture.obtenerEntidadUsuario().get()))
				.thenReturn(LoginRespTOFixture.loginRespTOConPhones());

		when(usuarioService.consultarUsuario(UsuarioLoginReqTOFixture.obtenerUsuarioLoginReq()))
				.thenThrow(NullPointerException.class).thenReturn(LoginRespTOFixture.loginRespTOConPhones());
	}

	@Test
	public void TestResfrescarToken() throws Exception {
		when(repository.findById(any(UUID.class))).thenReturn(UsuarioEntityFixture.obtenerEntidadUsuario());
		when(jwtUtil.getJWTToken(anyString())).thenReturn(anyString());
		when(repository.save(UsuarioEntityFixture.obtenerEntidadUsuario().get()))
				.thenReturn(UsuarioEntityFixture.obtenerEntidadUsuario().get());
		when(wrapper.getLoginRespTO(UsuarioEntityFixture.obtenerEntidadUsuario().get()))
				.thenReturn(LoginRespTOFixture.loginRespTOConPhones());
		when(usuarioService.refrescartoken(UsuarioLoginReqTOFixture.obtenerUsuarioLoginReq()))
				.thenReturn(LoginRespTOFixture.loginRespTOConPhones());
	}

	@Test
	public void consultarUsuario() throws Exception {
		when(repository.findById(any(UUID.class))).thenReturn(UsuarioEntityFixture.obtenerEntidadUsuario());
		when(wrapper.getLoginRespTO(UsuarioEntityFixture.obtenerEntidadUsuario().get()))
				.thenReturn(LoginRespTOFixture.loginRespTOConPhones());
		when(usuarioService.consultarUsuario(UsuarioLoginReqTOFixture.obtenerUsuarioLoginReq()))
				.thenReturn(LoginRespTOFixture.loginRespTOConPhones());

	}

}