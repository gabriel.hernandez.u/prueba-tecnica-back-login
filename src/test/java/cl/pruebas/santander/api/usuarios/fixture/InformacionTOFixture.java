package cl.pruebas.santander.api.usuarios.fixture;

import cl.pruebas.santander.api.usuarios.model.InformacionTO;

public class InformacionTOFixture {

	public static InformacionTO obtenerInfo() {
		InformacionTO fix = new InformacionTO();
		fix.setCodigoOperacion("000");
		fix.setGlosaOperacion("operacion exitosa");
		return fix;
	}

}
