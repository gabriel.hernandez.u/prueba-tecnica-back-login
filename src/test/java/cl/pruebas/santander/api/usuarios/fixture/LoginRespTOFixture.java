package cl.pruebas.santander.api.usuarios.fixture;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import cl.pruebas.santander.api.usuarios.model.LoginRespTO;
import cl.pruebas.santander.api.usuarios.model.Phones;

public class LoginRespTOFixture {

	public static LoginRespTO loginRespTO() {
		LoginRespTO fixture = new LoginRespTO();
		fixture.setActive(true);
		fixture.setCreated(LocalDateTime.now());
		fixture.setEmail("qwe@asd.cl");
		fixture.setId("sadasdad");
		fixture.setLastLogin(LocalDateTime.now());
		fixture.setName("asdasda");
		fixture.setPassword("sdasdasdasda");
		fixture.setToken("dasasdasdasd");
		return fixture;
	}
	
	public static LoginRespTO loginRespTOConPhones() {
		LoginRespTO fixture = new LoginRespTO();
		List<Phones> phones = new ArrayList<Phones>();
		phones.add(new Phones(1L,"santiago","chile"));
		phones.add(new Phones(2L,"curico","chile"));
		fixture.setActive(true);
		fixture.setCreated(LocalDateTime.now());
		fixture.setEmail("qwe@asd.cl");
		fixture.setId("sadasdad");
		fixture.setLastLogin(LocalDateTime.now());
		fixture.setName("asdasda");
		fixture.setPassword("sdasdasdasda");
		fixture.setToken("dasasdasdasd");
		fixture.setPhones(phones);
		return fixture;
	}

}
