package cl.pruebas.santander.api.usuarios.fixture;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import cl.pruebas.santander.api.usuarios.entity.UsuarioEntity;
import cl.pruebas.santander.api.usuarios.entity.UsuarioPhonesEntity;

public class UsuarioEntityFixture {

	public static Optional<UsuarioEntity> obtenerEntidadUsuario() {
		List<UsuarioPhonesEntity> phonesEn = new ArrayList<>();

		UsuarioEntity fixture = new UsuarioEntity();
		fixture.setActive(true);
		fixture.setCreated(LocalDateTime.now());
		fixture.setEmail("gabriel@asd.cl");
		fixture.setId(UUID.randomUUID());
		fixture.setLastLogin(LocalDateTime.now());
		fixture.setUser("gabriel");
		fixture.setPassword("sadasasdasdasdasdasdasdasd");

		phonesEn.add(new UsuarioPhonesEntity(1L, fixture, 1L, "abc", "cde"));

		fixture.setPhones(phonesEn);

		Optional<UsuarioEntity> opt = Optional.ofNullable(fixture);
		return opt;
	}
	
	public static Optional<UsuarioEntity> obtenerEntidadUsuarioNull() {
		List<UsuarioPhonesEntity> phonesEn = new ArrayList<>();

		UsuarioEntity fixture = new UsuarioEntity();
		

		Optional<UsuarioEntity> opt = Optional.ofNullable(fixture);
		return opt;
	}

}
